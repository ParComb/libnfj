(** Non-negative Floats modulo 2^20 *)

type t

val zero: t
val one: t

val add: t -> t -> t
val sub: t -> t -> t
val mul: t -> t -> t
val div: t -> t -> t

val binom: int -> int -> t

val of_float: float -> t
val of_int: int -> t

val pp: Format.formatter -> t -> unit
