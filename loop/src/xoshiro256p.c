// The xoshiro256+ PRNG (+ splitmax64 for seeding)
// Sources:
// - http://prng.di.unimi.it/
// - http://prng.di.unimi.it/xoshiro256plus.c
// - http://prng.di.unimi.it/splitmix64.c

#include <stdint.h>
#include "xoshiro256p.h"

// splitmix64

static uint64_t splitmix64(uint64_t* x) {
	uint64_t z = (*x += 0x9e3779b97f4a7c15);
	z = (z ^ (z >> 30)) * 0xbf58476d1ce4e5b9;
	z = (z ^ (z >> 27)) * 0x94d049bb133111eb;
	return z ^ (z >> 31);
}

// xoshiro256+

static inline uint64_t rotl(const uint64_t x, int k) {
	return (x << k) | (x >> (64 - k));
}

static uint64_t s[4] = {
  // equivalent to seeding with 0xDEADBEEFFEEBDAED
  2462350579978826501UL,
  4966698697614422118UL,
  17614650839593709205UL,
  12896803581221783479UL
};

uint64_t xoshiro256p_next(void) {
	const uint64_t result = s[0] + s[3];
	const uint64_t t = s[1] << 17;

	s[2] ^= s[0];
	s[3] ^= s[1];
	s[1] ^= s[2];
	s[0] ^= s[3];
	s[2] ^= t;
	s[3] = rotl(s[3], 45);

	return result;
}

// Seeding and state manipulation

void xoshiro256p_seed(uint64_t seed) {
  s[0] = splitmix64(&seed);
  s[1] = splitmix64(&s[0]);
  s[2] = splitmix64(&s[1]);
  s[3] = splitmix64(&s[2]);
}

static inline void copy_state(uint64_t to[4], const uint64_t from[4]) {
  to[0] = from[0];
  to[1] = from[1];
  to[2] = from[2];
  to[3] = from[3];
}

void xoshiro256p_getstate(uint64_t dest[4]) {copy_state(dest, s);}
void xoshiro256p_setstate(const uint64_t from[4]) {copy_state(s, from);}

double xoshiro256p_double(void) {
  return (xoshiro256p_next() >> 11) * 0x1.0p-53;
}
