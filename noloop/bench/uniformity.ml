open Libcbl

let distribution (type a) (f: unit -> a) nb_samplings =
  let tbl: (a, int) Hashtbl.t = Hashtbl.create 17 in
  let rec sample n =
    if n = 0 then ()
    else
      let x = f () in
      begin match Hashtbl.find_opt tbl x with
        | Some i -> Hashtbl.replace tbl x (i + 1)
        | None -> Hashtbl.add tbl x 1
      end;
      sample (n - 1)
  in
  sample nb_samplings;
  tbl


let example =
  let open Spp.Syntax in
  (atom "a" || (atom "b" + (atom "c" || atom "d")))
  >> (atom "e" + atom "f")
  >> (atom "g" || atom "h")

let () =
  let n =
    Spp.counting_poly (module Poly.P) example
    |> List.fold_left (fun acc (_, coeff) -> Z.add acc (Poly.Coeff.count coeff)) Z.zero
  in
  Format.printf "=> %a executions@." Z.pp_print n

let sample =
  let p = Spp.counting_poly (module Poly.P) example in
  fun () ->
    let choice = Sampling.global_choice p in
    Sampling.choice_free (Spp.apply_choice choice example)

let pp_execution =
  Format.pp_print_list
    ~pp_sep:(fun fmt () -> Format.fprintf fmt ",")
    Atom.pp

let () =
  let tbl = distribution sample 10_000 in
  Hashtbl.iter
    (fun ex n -> Format.printf "- %d : %a@." n pp_execution ex)
    tbl
