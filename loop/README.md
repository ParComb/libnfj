# libnfj

Random sampling library for NFJ programs.

## Build

You need to have `flint` (and its dependencies `gmp` and `mpfr`) installed on
your system to build libnfj. On Debian, Ubuntu, etc. installing `libflint-dev`
should be sufficient.
Optionally update the `config.mk` file and type `make` to build.
This produces a static library `libnfj.a`.

## Usage

To use `libnfj` in your code make sure your compiler finds `nfj.h` and
`libnfj.a` and build with `-lnfj -lflint -lmpfr -lgmp -lpthead`.

## Benchmarks

`make bench` will produce (but not run) some benchmarking scripts at the root
of the repository.

The `coverage/` folder contains the (C + OCaml) code used for an experiment
about the state-space coverage of two random sampling exploration strategies.
