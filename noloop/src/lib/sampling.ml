module Imap = Map.Make(Int)
module Choice = struct
  type t = Coeff.side Imap.t

  let empty = Imap.empty
  let add = Imap.add
  let get = Imap.find

  let rec apply choice (term: Nfj.t) = match term.desc with
    | Atom _ -> term
    | Seq (p, q) -> Nfj.seq (apply choice p) (apply choice q)
    | Par (p, q) -> Nfj.par (apply choice p) (apply choice q)
    | Plus (uid, p, q) ->
      begin match get uid choice with
        | Coeff.L -> apply choice p
        | Coeff.R -> apply choice q
      end
end

(** {2 Helpers} *)

(** A Bernoulli variable of parameter [n / (n + m)] *)
let bernoulli n m =
  Z.lt (RandUtils.bigint Z.(n + m)) n

(** Return a uniform suffling of two lists given their lenghs *)
let rec shuffle l1 n1 l2 n2 = match l1, l2 with
  | [], _ -> l2
  | _, [] -> l1
  | x1 :: l1', x2 :: l2' ->
    if Random.int (n1 + n2) < n1
    then x1 :: (shuffle l1' (n1 - 1) l2 n2)
    else x2 :: (shuffle l1 n1 l2' (n2 - 1))

(** {2 Size selection} *)

let sample_size gfun =
  let total = Array.fold_left (fun acc c -> Z.add c.Coeff.value acc) Z.zero gfun in
  let m = RandUtils.bigint total in
  let rec select i m =
    if Z.lt m gfun.(i).value then i
    else select (i + 1) (Z.sub m gfun.(i).value)
  in
  select 0 m

(** {2 Global choice sampling} *)

let sample_choice =
  let rec aux choice c = match c.Coeff.desc with
    | Const -> choice
    | Y (uid, side) -> Choice.add uid side choice
    | Prod (x, y) -> aux (aux choice x) y
    | Sum (x, y) ->
      if bernoulli x.value y.value
      then aux choice x
      else aux choice y
  in
  aux Choice.empty

(** {2 Choice-free sampling} *)

(** Sample an execution in a choice-free term. *)
let rec sample_choice_free term = match term.Nfj.desc with
  | Atom a -> [a]
  | Par (p, q) -> shuffle (sample_choice_free p) p.size (sample_choice_free q) q.size
  | Seq (p, q) -> sample_choice_free p @ sample_choice_free q
  | Plus _ -> invalid_arg "sample_choice_free"

(** {2 General case sampling} *)

(** Sample an execution of given length, given the gfun of executions *)
let unif_exec term gfun =
  let k = sample_size gfun in
  let e = gfun.(k) in
  let choice = sample_choice e in
  let term' = Choice.apply choice term in
  sample_choice_free term'
