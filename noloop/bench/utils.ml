(** {2 Pretty-printing utilities} *)

let pp_z_approx fmt n =
  let threshold = 50 in
  let offset = 20 in
  if Z.equal Z.zero n then Format.fprintf fmt "0"
  else
    let exponent = Z.log2 n in
    if exponent > threshold then
      let q = Z.shift_right n (Int.sub exponent offset) in
      let q = Z.to_float q /. (Float.pow 2. (float_of_int offset)) in
      let e = string_of_int exponent in
      let e =
        let strlen = String.length e in
        if strlen < 5 then e ^ String.make (5 - strlen) ' ' else e
      in
      Format.fprintf fmt "%.3f⋅2^%s" q e
    else
      Z.pp_print fmt n

(** {2 Generation of Nfj terms} *)

type target_size =
  | Exact of int
  | Window of int * int

let generate target =
  let size_min, size_max = match target with
    | Exact size -> size, size
    | Window (size_min, size_max) -> size_min, size_max
  in
  Generators.Nfj.gen ~size_min ~size_max

(** {2 Memory measurement} *)

module Mem = struct
  type t = int

  let as_bytes x = x * Sys.word_size / 8

  let pp fmt x =
    let n = as_bytes x in
    if n < 1024 then Format.fprintf fmt "%7dB" n
    else
      let n = float_of_int n /. 1024. in
      if n < 1024. then Format.fprintf fmt "%7.2fK" n
      else
        let n = n /. 1024. in
        Format.fprintf fmt "%7.2fM" n
end

let sizeof x = Obj.(reachable_words (repr x))

(** {2 Time measurement} *)

module Time = struct
  (* No child processes here *)
  type t = {
    wall: float;
    stime: float;
    utime: float;
  }

  let make () =
    let tms = Unix.times () in
    let wall = Unix.gettimeofday () in
    {
      wall;
      stime = tms.Unix.tms_stime;
      utime = tms.Unix.tms_utime;
    }

  let sub t1 t2 = {
    wall = t1.wall -. t2.wall;
    stime = t1.stime -. t2.stime;
    utime = t1.utime -. t2.utime;
  }

  let pp_f fmt f =
    if f >= 1. then Format.fprintf fmt "%6.2fs " f
    else
      let f = f *. 1000. in
      if f >= 1. then Format.fprintf fmt "%6.2fms" f
      else
        let f = f *. 1000. in
        if f >= 1. then Format.fprintf fmt "%6.2fus" f
        else
          let f = f *. 1000. in
          Format.fprintf fmt "%6.2fns" f


  let pp fmt {wall; utime; stime} =
    Format.fprintf fmt "%a(wall) %a(usr) %a(sys)"
      pp_f wall pp_f utime pp_f stime

  let pp_short fmt {wall; _} = pp_f fmt wall
end

let time thunk =
  let t0 = Time.make () in
  let res = thunk () in
  let t1 = Time.make () in
  res, Time.sub t1 t0
