#include <malloc.h>

#include "../nfj.h"
#include "recmeth.h"
#include "internals.h"

static void _nfj_rand_prefix(sp_t* q, flint_rand_t state, const nfj_t* p,
                             int len, pool* labels) {
  if (!len) return;
  int k;
  fmpz_t tmp;
  fmpz* lhs;
  fmpz_init(tmp);
  switch (p->constructor) {
    case ATOM:
      q->constructor = SP_ATOM;
      q->content.atom.id = p->content.id;
      q->content.atom.label = labels->buf[0];
      labels->buf++;
      break;
    case PLUS:
      if (bernoulli(state,
                    fmpz_poly_get_coeff_ptr(p->content.args._0->prefix_gfun, len),
                    fmpz_poly_get_coeff_ptr(p->prefix_gfun, len)))
        _nfj_rand_prefix(q, state, p->content.args._0, len, labels);
      else
        _nfj_rand_prefix(q, state, p->content.args._1, len, labels);
      break;
    case PAR:
      k = recmeth_colprod(state,
                          fmpz_poly_get_coeff_ptr(p->prefix_gfun, len),
                          p->content.args._0->prefix_gfun,
                          p->content.args._1->prefix_gfun,
                          len);
      if (k == 0)
        _nfj_rand_prefix(q, state, p->content.args._1, len, labels);
      else if (k == len)
        _nfj_rand_prefix(q, state, p->content.args._0, len, labels);
      else {
        pool_shuffle(state, len, k, labels);
        q->constructor = SP_PAR;
        q->content.args._0 = malloc(sizeof(sp_t));
        q->content.args._1 = malloc(sizeof(sp_t));
        _nfj_rand_prefix(q->content.args._0, state, p->content.args._0, k, labels);
        _nfj_rand_prefix(q->content.args._1, state, p->content.args._1, len - k, labels);
      }
      break;
    case SEQ:
      lhs = fmpz_poly_get_coeff_ptr(p->content.args._0->prefix_gfun, len);
      if (bernoulli(state, lhs, fmpz_poly_get_coeff_ptr(p->prefix_gfun, len)))
        _nfj_rand_prefix(q, state, p->content.args._0, len, labels);
      else {
        fmpz_set(tmp, fmpz_poly_get_coeff_ptr(p->prefix_gfun, len));
        if (lhs != NULL) fmpz_sub(tmp, tmp, lhs);
        k = recmeth_prod_nonzero(state, tmp,
                                 p->content.args._1->prefix_gfun,
                                 p->content.args._0->gfun, len);
        if (k == len) {
          _nfj_rand_prefix(q, state, p->content.args._1, len, labels);
        } else {
          q->constructor = SP_SEQ;
          q->content.args._0 = malloc(sizeof(sp_t));
          q->content.args._1 = malloc(sizeof(sp_t));
          _nfj_rand_exec(q->content.args._0, state, p->content.args._0, len - k, labels);
          _nfj_rand_prefix(q->content.args._1, state, p->content.args._1, k, labels);
        }
      }
      break;
    case LOOP:
      k = recmeth_prod_nonzero(state,
                               fmpz_poly_get_coeff_ptr(p->prefix_gfun, len),
                               p->content.arg->prefix_gfun,
                               p->gfun,
                               len);
      if (k == len)
        _nfj_rand_prefix(q, state, p->content.arg, len, labels);
      else {
        q->constructor = SP_SEQ;
        q->content.args._0 = malloc(sizeof(sp_t));
        q->content.args._1 = malloc(sizeof(sp_t));
        _nfj_rand_exec(q->content.args._0, state, p, len - k, labels);
        _nfj_rand_prefix(q->content.args._1, state, p->content.arg, k, labels);
      }
      break;
    case ZERO:
      break;
    fmpz_clear(tmp);
  }
}

void nfj_rand_prefix(sp_t* q, flint_rand_t state, const nfj_t* p, int len) {
  int* buf = malloc(sizeof(int) * len);
  int* backup = malloc(sizeof(int) * len);
  pool labels = {.buf = buf, .backup = backup};

  for (int i = 0; i < len; i++) labels.buf[i] = i + 1;
  _nfj_rand_prefix(q, state, p, len, &labels);

  free(buf);
  free(backup);
}
