#include <stdio.h>
#include <flint/fmpz_poly.h>
#include <flint/fmpz.h>

#include "fmpz_poly_extra.h"
#include "../nfj.h"

void nfj_compute_prefix_gfuns(nfj_t* p, slong n) {
  if (fmpz_poly_length(p->prefix_gfun) > 0) return;

  switch (p->constructor) {
    case ZERO:
      fmpz_poly_one(p->prefix_gfun);
      break;
    case ATOM:
      fmpz_poly_set_coeff_si(p->prefix_gfun, 1, 1);
      fmpz_poly_set_coeff_si(p->prefix_gfun, 0, 1);
      break;
    case PAR:
      nfj_compute_prefix_gfuns(p->content.args._0, n);
      nfj_compute_prefix_gfuns(p->content.args._1, n);
      fmpz_poly_extra_colprodlow(p->prefix_gfun,
                                 p->content.args._0->prefix_gfun,
                                 p->content.args._1->prefix_gfun, n + 1);
      break;
    case SEQ:
      nfj_compute_prefix_gfuns(p->content.args._0, n);
      nfj_compute_prefix_gfuns(p->content.args._1, n);
      fmpz_poly_set_coeff_si(p->content.args._1->prefix_gfun, 0, 0); // temp
      fmpz_poly_mullow(p->prefix_gfun,
                       p->content.args._0->gfun,
                       p->content.args._1->prefix_gfun, n + 1);
      fmpz_poly_set_coeff_si(p->content.args._1->prefix_gfun, 0, 1); // undo
      fmpz_poly_add(p->prefix_gfun,
                    p->prefix_gfun,
                    p->content.args._0->prefix_gfun);
      break;
    case PLUS:
      nfj_compute_prefix_gfuns(p->content.args._0, n);
      nfj_compute_prefix_gfuns(p->content.args._1, n);
      fmpz_poly_add(p->prefix_gfun,
                    p->content.args._0->prefix_gfun,
                    p->content.args._1->prefix_gfun);
      fmpz_poly_set_coeff_si(p->prefix_gfun, 0, 1);
      break;
    case LOOP:
      nfj_compute_prefix_gfuns(p->content.arg, n);
      fmpz_poly_set_coeff_si(p->content.arg->prefix_gfun, 0, 0); // temp
      fmpz_poly_mullow(p->prefix_gfun,
                       p->gfun,
                       p->content.arg->prefix_gfun, n + 1);
      fmpz_poly_set_coeff_si(p->content.arg->prefix_gfun, 0, 1); // undo
      fmpz_poly_set_coeff_si(p->prefix_gfun, 0, 1);
      break;
  }
}

