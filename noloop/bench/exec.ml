open Libnfj
open Utils

let print_header () =
  Format.printf " size # executions  mem.size     Gfun UnifExec      IQR@."

let total gfun =
  Array.fold_right
    (fun coeff tot -> Z.add tot coeff.Coeff.value)
    gfun
    Z.zero

let time_gfun term =
  let gfun = ref [||] in
  let times =
    Array.init 7
      (fun _ ->
        let start = Time.make () in
        gfun := Nfj.gfun term;
        let end_ = Time.make () in
        (Time.sub end_ start).utime)
  in
  Array.fast_sort Float.compare times;
  !gfun, times.(3)

let time_gen term gfun =
  let times =
    Array.init 101
      (fun _ ->
        let e, t = time (fun () -> Sampling.unif_exec term gfun) in
        ignore (Sys.opaque_identity e);
        t.Time.utime)
  in
  Array.fast_sort Float.compare times;
  times.(50), times.(75) -. times.(25)

let bench (size, term) =
  Format.printf "%5d @?" size;
  let gf, gftime = time_gfun term in
  let count = total gf in
  let mem = sizeof gf in
  Format.printf "%a %a %f @?" pp_z_approx count Mem.pp mem gftime;
  let exctime, iqr = time_gen term gf in
  Format.printf "%f %f@." exctime iqr

(* ---------------------------------- *)

let () =
  let config = [
    Exact 100;
    Exact 200;
    Exact 500;
    Exact 1000;
    Exact 2000;
    Exact 3000;
    Exact 5000;
  ] in
  Format.printf "Generating test programs… @?";
  let terms = List.map generate config in
  Format.printf "done@.";
  Format.printf "=> Starting benchmark@.";
  print_header ();
  List.iter bench terms;
