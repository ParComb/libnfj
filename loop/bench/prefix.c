#include <stdio.h>
#include <time.h>
#include <flint/flint.h> // flint_rand_t
#include "../nfj.h"
#include "util.h"

#define repeat 7

const long int sizes[] = {
  100,
  500,
  1000,
  2000,
  5000,
};

static const long int lens[] = {
  500,
  1000,
  3000,
};

static const int nb_tests = sizeof(sizes) / sizeof(long int);
static const int nb_lens = sizeof(lens) / sizeof(long int);

void prepare(nfj_t* progs[]) {
  printf("Generating test programs… ");
  fflush(stdout);
  for (int i = 0; i < nb_tests; i++) {
    progs[i] = malloc(sizeof(nfj_t));
    nfj_random(progs[i], sizes[i], sizes[i]);
  }
  printf("done.\n");
  fflush(stdout);
}

int cmp_double(const void* a, const void* b) {
  if (*(double*)a > *(double*)b)
    return 1;
  else if (*(double*)a < *(double*)b)
    return -1;
  else
    return 0;
}

void bench(flint_rand_t state, nfj_t* p, long int size, long int n) {
  clock_t start, end;
  fmpz_t total;
  fmpz_init(total);
  sp_t* e;
  mp_size_t memsize = 0;

  printf("%4ld %5ld ", size, n);
  fflush(stdout);

  /* Counting */
  double times[repeat];
  for (int i = 0; i < repeat; i++) {
    start = clock();
    nfj_compute_gfuns(p, n);
    nfj_compute_prefix_gfuns(p, n);
    end = clock();
    times[i] = (double)(end - start) / CLOCKS_PER_SEC;

    if (i == 0) {
      fmpz_poly_get_coeff_fmpz(total, p->prefix_gfun, n);
      memsize = nfj_gfuns_size(p);
    }
    if (i < repeat - 1) {
      nfj_reset_gfuns(p);
      nfj_reset_prefix_gfuns(p);
      flint_cleanup();
    }
  }
  qsort(times, repeat, sizeof(double), cmp_double);

  fmpz_print_pretty(total);
  printf(" ");
  print_size(memsize);
  printf(" %lf", times[repeat / 2]);
  fflush(stdout);

  /* Random sampling */
  double rand_times[101];
  for (int i = 0; i < 101; i++) {
    e = malloc(sizeof(sp_t));
    start = clock();
    nfj_rand_prefix(e, state, p, n);
    end = clock();
    sp_clear_all(e);
    rand_times[i] = (double)(end - start) / CLOCKS_PER_SEC;
  }
  qsort(rand_times, 101, sizeof(double), cmp_double);
  printf(" %lf %lf\n", rand_times[50], rand_times[75] - rand_times[25]);

  fmpz_clear(total);
  nfj_reset_gfuns(p);
  nfj_reset_prefix_gfuns(p);
}

int main() {
  flint_rand_t state;
  flint_randinit(state);
  flint_randseed(state, 0xdeadbeefcafeb0b0, 0xa1922b1201209e12);

  nfj_t* progs[nb_tests];
  prepare(progs);

  printf("Running benchmark:\n");
  printf("size   len sizeof(st.space)     sizeof PrefGfun UnifPref      IQR\n");
  fflush(stdout);

  for (int i = 0; i < nb_tests; i++) {
    for (int j = 0; j < nb_lens; j++) {
      bench(state, progs[i], sizes[i], lens[j]);
    }
  }

  for (int i = 0; i < nb_tests; i++) {
    nfj_clear_all(progs[i]);
  }

  return 0;
}

