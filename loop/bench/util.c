#include <stdio.h>
#include <flint/fmpz.h>
#include <flint/fmpz_poly.h>
#include "../nfj.h"


void fmpz_print_pretty(const fmpz_t n) {
  if (fmpz_fits_si(n)) {
    printf("%-20ld", fmpz_get_si(n));
  } else {
    slong exp;
    double r = fmpz_get_d_2exp(&exp, n);
    printf("%lf⋅2^%-7ld", r * 2., exp - 1);
  }
}

mp_size_t fmpz_poly_size(const fmpz_poly_t poly) {
  mp_size_t s = 0;
  for (slong i = 0; i < fmpz_poly_length(poly); i++) {
    s += fmpz_size(fmpz_poly_get_coeff_ptr(poly, i));
  }
  return s;
}

mp_size_t nfj_gfuns_size(const nfj_t* p) {
  mp_size_t s = fmpz_poly_size(p->gfun);
  s += fmpz_poly_size(p->prefix_gfun);
  switch (p->constructor) {
    case ZERO:
    case ATOM:
      break;
    case LOOP:
      s += nfj_gfuns_size(p->content.arg);
      break;
    default:
      s += nfj_gfuns_size(p->content.args._0);
      s += nfj_gfuns_size(p->content.args._1);
  }
  return s;
}

void print_size(mp_size_t size) {
  double s = size * sizeof(mp_size_t);
  if (s < 1024) {
    printf("%7.0lfB", s);
    return;
  }

  s = s / 1024;
  if (s < 1024) {
    printf("%7.2lfK", s);
    return;
  }

  s = s / 1024;
  if (s < 1024) {
    printf("%7.2lfM", s);
    return;
  }

  s = s / 1024;
  printf("%7.2lfG", s);
}

void measure_state_space(fmpz_t res, const fmpz_poly_t poly, slong n) {
  fmpz_zero(res);

  for (slong i = 0; i <= n; i++) {
    fmpz_add(res, res, fmpz_poly_get_coeff_ptr(poly, i));
  }
}

