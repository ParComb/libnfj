(** {2 Counter: a multi-set structure for storing probabilities} *)

module Counter = struct
  module Fmap = Map.Make(Float)

  type t = Z.t Fmap.t

  let get f counter =
    match Fmap.find_opt f counter with
    | None -> Z.zero
    | Some n -> n

  let incr f counter =
    let n = get f counter in
    Fmap.add f (Z.add n Z.one) counter

  let cardinal counter =
    Fmap.fold (fun _ n tot -> Z.add n tot) counter Z.zero

  let sum counter =
    Fmap.fold (fun p n tot -> tot +. (p *. Z.to_float n)) counter 0.

  let empty = Fmap.empty

  let complete counter =
    (* The sum of all the probabilities of the counter might be < 1. This
     * happens when when some exploration paths lead to dead-ends. Add an extra
     * articifial prefix to the counter, this special prefix represents the
     * dead-end scenario. *)
    let deadend_proba = 1. -. sum counter in
    if deadend_proba > 0. then incr deadend_proba counter
    else counter

  let to_list counter =
    Fmap.to_seq counter |> List.of_seq

  let pp fmt counter =
    let pp_sep fmt () = Format.fprintf fmt ", " in
    let pp_binding fmt (p, n) = Format.fprintf fmt "%F -> %a" p Z.pp_print n in
    let bindings = Fmap.bindings counter in
    Format.fprintf fmt "Counter(%a)"
      (Format.pp_print_list ~pp_sep pp_binding) bindings
end


(** {2 Prefix exploration} *)

let prefix_probabilities ~max_len p =
  let counters = Array.make (max_len + 1) Counter.empty in

  let rec aux len proba p =
    if len > max_len then ()
    else if p = Nfj.Zero then ()
    else
      let next = Nfj.one_step p in
      let proba =  proba /. float_of_int (List.length next) in
      List.iter
        (fun p' ->
          counters.(len) <- Counter.incr proba counters.(len);
          aux (len + 1) proba p')
        next
  in
  aux 1 1. p;
  Array.iteri (fun i c -> counters.(i) <- Counter.complete c) counters;
  counters

(** {2 The Coupon Collector Problem} *)

(** {3 Uniform case} *)

let uniform_ccp ~cov_ratio k =
  let rec aux acc i =
    if Z.lt k i then acc
    else
      aux (acc +. (1. /. (Z.to_float i))) (Z.succ i)
  in
  let start =
    let open Z in
    let j = of_float (to_float k *. cov_ratio) in
    k - j + one
  in
  if cov_ratio = 1. then assert (start = Z.one);
  (aux 0. start) *. (Z.to_float k)

(** {3 General case} *)

let rec dec_range n () =
  if n < 0 then Seq.Nil
  else Seq.Cons (n, dec_range (n - 1))

module Partition = struct
  type t = int list

  let pp fmt : t -> unit =
    Format.fprintf fmt "[%a]"
      (Format.pp_print_list
        ~pp_sep:(fun fmt () -> Format.pp_print_string fmt ", ")
        Format.pp_print_int)

  let rec iter (bounds: t) (n: int) : t Seq.t =
    match bounds with
    | [] ->
      if n = 0 then Seq.return []
      else Seq.empty
    | b0 :: bs ->
      Seq.flat_map
        (fun k -> Seq.map (fun p -> k :: p) (iter bs (n - k)))
        (dec_range (min b0 n))
end

let term_of_partition counter (partition: Partition.t) : float =
  let sum =
    List.fold_left2
      (fun sum (pi, _) k -> sum +. (Float.of_int k *. pi))
      0.
      counter
      partition
  in
  1. /. (1. -. sum)

let multiplicator bounds (partition: Partition.t) =
  List.fold_left2
    (fun m ni ki -> F.mul m (F.binom ni ki))
    F.one
    bounds
    partition

(** Solution in the general case *)
let coupon_collector ~cov_ratio counter =
  (* Format.printf "couter = %a@." Counter.pp counter; *)
  let m = Counter.cardinal counter in
  let m = Z.to_int m in

  let counter = Counter.to_list counter in
  let bounds = List.map (fun (_, z) -> Z.to_int z) counter in

  let j = int_of_float (float_of_int m *. cov_ratio) in
  if cov_ratio = 1. then assert (j = m);

  let q_term q =
    let sum =
      Seq.fold_left
        (fun s partition ->
          let term = term_of_partition counter partition |> F.of_float in
          let m = multiplicator bounds partition in
          F.add s (F.mul m term))
        F.zero
        (Partition.iter bounds q)
    in
    let b = F.binom (m - q - 1) (m - j) in
    F.mul sum b
  in

  let pos_part = ref F.zero in
  let neg_part = ref F.zero in
  for q = 0 to j - 1 do
    let term = q_term q in
    if (j - 1 - q) mod 2 = 0 then
      pos_part := F.add !pos_part term
    else
      neg_part := F.add !neg_part term
  done;
  F.sub !pos_part !neg_part


(** {2 Computation of \ref{table:prefixcov} for the paper} *)

let print_row title pp_elem values =
  Format.printf "%s & " title;
  let pp_sep fmt () = Format.fprintf fmt " & " in
  Format.pp_print_list ~pp_sep pp_elem Format.std_formatter values;
  Format.printf "\\\\@."

let main () =
  let max_len, cov_ratio =
    if Array.length Sys.argv != 3 then begin
      Format.eprintf "usage: %s N COVERAGE_RATIO@." Sys.argv.(0);
      exit 1
    end;
    (int_of_string Sys.argv.(1), Float.of_string Sys.argv.(2))
  in

  let coefficients =
    input_line stdin
    |> String.trim
    |> String.split_on_char ' '
    |> List.map Z.of_string
  in

  let lexbuf = Lexing.from_channel stdin in
  let p = Parser.file Lexer.token lexbuf in

  (* Coupon Collector Problem *)
  let counters = prefix_probabilities ~max_len p in
  let isotropic = List.init max_len (fun i -> coupon_collector ~cov_ratio counters.(i + 1)) in

  let pp_nb_pref fmt n =
    let j = int_of_float (Z.to_float n *. cov_ratio) in
    Format.fprintf fmt "%a|%d" Z.pp_print n j
  in

  print_row "Prefix length" Format.pp_print_int (List.init max_len Int.succ);
  print_row "Number of prefixes" pp_nb_pref (List.tl coefficients);
  print_row "Isotropic" F.pp isotropic;
  let global_uniform = List.map (uniform_ccp ~cov_ratio) (List.tl coefficients) in
  print_row "Global uniformity" Format.pp_print_float global_uniform;
  let speedup = List.map2 (fun u i -> let u = F.of_float u in F.(mul (of_int 100) (div (sub i u) u))) global_uniform isotropic in
  print_row "Speedup" (fun fmt -> Format.fprintf fmt "%a%%" F.pp) speedup;
  ()

let () = main ()
