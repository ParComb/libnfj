open Libnfj
open Options


let report_loc filename (b,e) =
  let open Lexing in
  let l = b.pos_lnum in
  let fc = b.pos_cnum - b.pos_bol + 1 in
  let lc = e.pos_cnum - b.pos_bol + 1 in
  Format.eprintf "File \"%s\", line %d, characters %d-%d:@." filename l fc lc

let parse_from_channel options ic filename =
  let lexbuf = Lexing.from_channel ic in
  try Parser.file Lexer.token lexbuf with
  | Parser.Error ->
    if options.input <> None then close_in_noerr ic;
    let start = Lexing.lexeme_start_p lexbuf in
    let stop = Lexing.lexeme_end_p lexbuf in
    report_loc filename (start, stop) ;
    Format.eprintf "syntax error\n@.";
    exit 1
  | exn ->
    if options.input <> None then close_in_noerr ic;
    raise exn

let parse_input options =
  match options.input with
  | Some filename ->
    let ic = open_in filename in
    let term = parse_from_channel options ic filename in
    close_in ic;
    term
  | None -> parse_from_channel options stdin ""
