#include "flint/flint.h" // FLINT_MIN
#include "flint/fmpz.h"
#include "flint/fmpz_poly.h"
#include "fmpz_poly_extra.h"

void _fmpz_poly_extra_seq_newton(fmpz* res, const fmpz* poly, slong len, slong n) {
  for (slong i = 1; i < n; i++) fmpz_zero(res + i);
  fmpz_one(res);
  if (n == 1) return;

  fmpz_poly_t t1, t2;
  fmpz_poly_init2(t1, n);
  fmpz_poly_init2(t2, n);

  int cur_n = 1;
  while (cur_n < n) {
    int next_n = FLINT_MIN(cur_n << 1, n);
    int len_t1 = FLINT_MIN(next_n, len + cur_n - 1);

    if (len_t1 > 1) {
      if (cur_n >= len - 1)
        _fmpz_poly_mullow(t1->coeffs + 1, res, cur_n, poly + 1, len - 1, len_t1 - 1);
      else
        _fmpz_poly_mullow(t1->coeffs + 1, poly + 1, len - 1, res, cur_n, len_t1 - 1);
    }
    for (slong k = 1; k < len_t1; k++)
      fmpz_sub(t1->coeffs + k, t1->coeffs + k, res + k);

    _fmpz_poly_mullow(t2->coeffs, t1->coeffs, len_t1, res, cur_n, next_n);
    for (slong k = 1; k < next_n; k++)
      fmpz_add(res + k, res + k, t2->coeffs + k);

    cur_n = next_n;
  }

  fmpz_poly_clear(t1);
  fmpz_poly_clear(t2);
}

void fmpz_poly_extra_seq_newton(fmpz_poly_t res, const fmpz_poly_t poly, slong n) {
  if (res == poly) {
    fmpz_poly_t t;
    fmpz_poly_init2(t, n);
    _fmpz_poly_extra_seq_newton(t->coeffs, poly->coeffs, poly->length, n);
    fmpz_poly_swap(t, res);
    fmpz_poly_clear(t);
  } else {
    fmpz_poly_fit_length(res, n);
    _fmpz_poly_extra_seq_newton(res->coeffs, poly->coeffs, poly->length, n);
  }

  _fmpz_poly_set_length(res, n);
  _fmpz_poly_normalise(res);
}
