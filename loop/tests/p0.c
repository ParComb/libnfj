// ----------------------------------------------- //
// The running example P0 from the submitted paper //
// ----------------------------------------------- //

#include <stdio.h>
#include <time.h>
#include <flint/flint.h>
#include <flint/fmpz.h>
#include <flint/fmpz_poly.h>
#include "../nfj.h"

#define ZERO(z) nfj_t* z = malloc(sizeof(nfj_t)); nfj_init_zero(z);
#define ATOM(a) nfj_t* a = malloc(sizeof(nfj_t)); nfj_init_atom(a);
#define PAR(p, q, r) nfj_t* p = malloc(sizeof(nfj_t)); nfj_init_par(p, q, r);
#define PLUS(p, q, r) nfj_t* p = malloc(sizeof(nfj_t)); nfj_init_plus(p, q, r);
#define SEQ(p, q, r) nfj_t* p = malloc(sizeof(nfj_t)); nfj_init_seq(p, q, r);
#define LOOP(p, q) nfj_t* p = malloc(sizeof(nfj_t)); nfj_init_loop(p, q);


nfj_t* make_example() {
  ATOM(a) ATOM(b) ATOM(c)
  PAR(bc, b, c)
  PLUS(abc, a, bc)
  LOOP(l1, abc)

  ATOM(d) ZERO(z)
  PLUS(dz, d, z)
  PAR(p1, l1, dz)

  LOOP(l2, p1)

  ATOM(e) ATOM(f) ATOM(g)
  PAR(fg, f, g)
  PLUS(p2, e, fg)

  SEQ(res, l2, p2)
  return res;
}

const int default_n = 5000;

void usage(char progname[], const nfj_t* p0) {
  fprintf(stderr, "usage: %s [N]\n\n", progname);
  fprintf(stderr, "Compute the number of executions of length <= N of the program\n");
  fprintf(stderr, "P0 = ");
  nfj_pretty_print(stderr, p0);
  fprintf(stderr, "\n(By default N = %d)\n", default_n);
  exit(1);
}

int cmd_line_parse(int argc, char* argv[], const nfj_t* p0) {
  if (argc > 2) usage(argv[0], p0);
  if (argc == 1) return default_n;

  int n = atoi(argv[1]);
  if (n <= 0) usage(argv[0], p0);
  return n;
}

int main(int argc, char* argv[]) {
  nfj_t* p0 = make_example();
  int n = cmd_line_parse(argc, argv, p0);

  clock_t start = clock();
  nfj_compute_gfuns(p0, n);
  clock_t end = clock();

  fmpz_t one, total;
  fmpz_init(one); fmpz_one(one);
  fmpz_init(total);
  fmpz_poly_evaluate_fmpz(total, p0->gfun, one);

  printf("P0 has ");
  fmpz_fprint(stdout, total);
  printf(" executions of length <= %d.\n", n);
  printf(
    "This number has been computed in %lf seconds.\n",
    (double)(end - start) / CLOCKS_PER_SEC
  );

  return 0;
}
