let rec rand_bits nb =
  let bs = Z.of_int (Random.bits ()) in
  if nb <= 30 then bs
  else
    let bs' = rand_bits (nb - 30) in
    Z.logor (Z.shift_left bs' 30) bs

let bigint =
  let rec gen n =
    let size_of_n = Z.log2 n + 1 in
    let bits = rand_bits size_of_n in
    let size_of_bits = 30 * ((size_of_n + 29) / 30) in
    let r = Z.rem bits n in
    if Z.(bits - r) > Z.((shift_left one size_of_bits) - n + one)
    then gen n
    else r
  in
  fun n ->
    if n <= Z.zero then invalid_arg "RandUtils.bigint"
    else gen n
