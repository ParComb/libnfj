(** Non-deterministic Fork-Join programs with loops. *)

type uid = int

type t = {size: int; desc: desc}
and desc =
  | Atom of Atom.t
  | Par of t * t
  | Seq of t * t
  | Plus of uid * t * t

val atom: Atom.t -> t
val par: t -> t -> t
val seq: t -> t -> t
val plus: uid -> t -> t -> t

(** {2 Generating function} *)

val gfun: t -> Poly.t

(** {2 Pretty printing} *)

val pp: Format.formatter -> t -> unit
