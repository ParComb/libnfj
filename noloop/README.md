# nanoCBL

## To build locally

```
opam install . --with-test --with-doc --deps-only
make
```

The library files are located in `lib/` and the executable is in `bin/`

## To build and install in the current switch

```
opam install .
```

## To build and access the documentation

```
make doc
open doc/index.html
```
