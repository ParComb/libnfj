#include <assert.h>

#include <flint/flint.h>
#include <flint/fmpz.h>
#include <flint/fmpz_poly.h>
#include "../src/fmpz_poly_extra.h"

void test_z() {
  fmpz_poly_t z;
  fmpz_poly_init(z);
  fmpz_poly_set_coeff_si(z, 1, 1);

  fmpz_poly_t p;
  fmpz_poly_init(p);
  fmpz_poly_extra_seq_basic(p, z, 100);
  assert(fmpz_poly_length(p) == 100);
  for (int i = 0; i < 100; i++)
    assert(fmpz_is_one(fmpz_poly_get_coeff_ptr(p, i)));

  fmpz_poly_clear(p);
  fmpz_poly_init(p);
  fmpz_poly_extra_seq_newton(p, z, 100);
  assert(fmpz_poly_length(p) == 100);
  for (int i = 0; i < 100; i++)
    assert(fmpz_is_one(fmpz_poly_get_coeff_ptr(p, i)));

  fmpz_poly_clear(z);
  fmpz_poly_clear(p);
}

void test_rand() {
  flint_rand_t state;
  flint_randinit(state);
  flint_randseed(state, 0x4242deadbeef3737, 0xdead17171717beef);

  fmpz_poly_t p;
  fmpz_poly_init(p);
  fmpz_poly_t res1, res2;
  fmpz_poly_init(res1);
  fmpz_poly_init(res2);

  slong max_size = 100;
  slong size = 80;
  for (int i = 0; i < 50; i++) {
    fmpz_poly_randtest(p, state, max_size, 100);
    fmpz_poly_extra_seq_basic(res1, p, size);
    fmpz_poly_extra_seq_newton(res2, p, size);
    assert(fmpz_poly_equal(res1, res2));

    if (fmpz_poly_length(p) > 0) {
      fmpz_set_si(fmpz_poly_get_coeff_ptr(p, 0), -1);
      fmpz_poly_neg(p, p);
      fmpz_poly_mullow(res1, res1, p, size);
      assert(fmpz_poly_is_one(res1));
    }
  }

  fmpz_poly_clear(p);
  fmpz_poly_clear(res1);
  fmpz_poly_clear(res2);
  flint_randclear(state);
}

int main() {
  test_z();
  test_rand();

  return 0;
}

