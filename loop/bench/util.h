#ifndef BENCH_UTIL_H
#define BENCH_UTIL_H 1

#include <flint/flint.h>
#include <flint/fmpz.h>
#include "../nfj.h"

void fmpz_print_pretty(const fmpz_t n);
mp_size_t nfj_gfuns_size(const nfj_t*);
void print_size(mp_size_t);
void measure_state_space(fmpz_t, const fmpz_poly_t, slong);

#endif

