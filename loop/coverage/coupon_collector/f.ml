type t = Z.t

(* precision *)
let offset = 8192

let of_float f =
  let x, n = Float.frexp f in
  let x' = x *. 0x1.0p+53 in
  let x' = Z.of_float x' in
  let s = n - 53 + offset in
  assert (s >= 0);
  Z.shift_left x' s

let of_bigint n =
  Z.shift_left n offset

let of_int n =
  of_bigint (Z.of_int n)

let zero = of_int 0
let one = of_int 1

let add = Z.add
let sub = Z.sub

let mul x y =
  Z.(shift_right (mul x y) offset)

let div x y =
  let x' = Z.shift_left x offset in
  Z.div x' y

let binom =
  let rec aux n k =
    if k = 0 then Z.one
    else
      Z.divexact
        (Z.mul (aux (n - 1) (k - 1)) (Z.of_int n))
        (Z.of_int k)
  in
  fun n k ->
    of_bigint (aux n (min k (n - k)))

let pp fmt n =
  let e = Z.shift_right n offset in
  let f = Z.(n land (sub (shift_left one offset) one)) in
  let f' = Z.shift_right f (offset - 53) in
  let f' = Z.to_float f' *. 0x1.0p-53 in
  let s = Float.to_string f' in
  let s = String.sub s 2 (String.length s - 2) in
  Format.fprintf fmt "%a.%s" Z.pp_print e s
