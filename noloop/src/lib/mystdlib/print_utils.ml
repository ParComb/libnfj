let rec pp_superscript fmt n =
  if n < 10 then
    if n > 3 || n = 0 then
      Format.fprintf fmt "%s%c" "\226\129" (char_of_int (176 + n))
    else if n = 1 then
      Format.pp_print_string fmt "\194\185"
    else
      Format.fprintf fmt "%s%c" "\194" (char_of_int (176 + n))
  else
    Format.fprintf fmt "%a%a" pp_superscript (n / 10) pp_superscript (n mod 10)
let pp_superscript fmt n =
  if n >= 0 then pp_superscript fmt n
  else Format.fprintf fmt "%s%a" "⁻" pp_superscript n


let rec pp_subscript fmt n =
  if n < 10 then
    Format.fprintf fmt "%s%c" "\226\130" (128 + n |> char_of_int)
  else
    Format.fprintf fmt "%a%a" pp_subscript (n / 10) pp_subscript (n mod 10)
let pp_subscript fmt n =
  if n >= 0 then pp_subscript fmt n
  else Format.fprintf fmt "%s%a" "₋" pp_subscript n
