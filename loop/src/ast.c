#include <stdio.h>
#include "../nfj.h"

// Note: we allocate a polynomial for each ZERO constructor.
// pro: it keeps on consistent interface in the AST
// con: it's a (little) waste of space.

// The same remarks holds for ATOMs, they all have the same generating
// function.

void nfj_init_zero(nfj_t* p) {
  p->constructor = ZERO;
  fmpz_poly_init(p->gfun);
  fmpz_poly_init(p->prefix_gfun);
  p->content.id = 0;
}

static int _nfj_atom_counter = -1;
void nfj_init_atom(nfj_t* p) {
  _nfj_atom_counter += 1;
  p->constructor = ATOM;
  fmpz_poly_init(p->gfun);
  fmpz_poly_init(p->prefix_gfun);
  p->content.id = _nfj_atom_counter;
}

void nfj_init_par(nfj_t* p, nfj_t* q, nfj_t* r) {
  p->constructor = PAR;
  fmpz_poly_init(p->gfun);
  fmpz_poly_init(p->prefix_gfun);
  p->content.args._0 = q;
  p->content.args._1 = r;
}

void nfj_init_seq(nfj_t* p, nfj_t* q, nfj_t* r) {
  p->constructor = SEQ;
  fmpz_poly_init(p->gfun);
  fmpz_poly_init(p->prefix_gfun);
  p->content.args._0 = q;
  p->content.args._1 = r;
}

void nfj_init_plus(nfj_t* p, nfj_t* q, nfj_t* r) {
  p->constructor = PLUS;
  fmpz_poly_init(p->gfun);
  fmpz_poly_init(p->prefix_gfun);
  p->content.args._0 = q;
  p->content.args._1 = r;
}

void nfj_init_loop(nfj_t* p, nfj_t* q) {
  p->constructor = LOOP;
  fmpz_poly_init(p->gfun);
  fmpz_poly_init(p->prefix_gfun);
  p->content.arg = q;
}

void nfj_pretty_print(FILE *f, const nfj_t* p) {
  switch (p->constructor) {
    case ZERO:
      fprintf(f, "0");
      break;
    case ATOM:
      fprintf(f, "a%d", p->content.id);
      break;
    case PAR:
      fprintf(f, "(");
      nfj_pretty_print(f, p->content.args._0);
      fprintf(f, " || ");
      nfj_pretty_print(f, p->content.args._1);
      fprintf(f, ")");
      break;
    case SEQ:
      fprintf(f, "(");
      nfj_pretty_print(f, p->content.args._0);
      fprintf(f, "; ");
      nfj_pretty_print(f, p->content.args._1);
      fprintf(f, ")");
      break;
    case PLUS:
      fprintf(f, "(");
      nfj_pretty_print(f, p->content.args._0);
      fprintf(f, " + ");
      nfj_pretty_print(f, p->content.args._1);
      fprintf(f, ")");
      break;
    case LOOP:
      nfj_pretty_print(f, p->content.arg);
      fprintf(f, "*");
      break;
  }
}

void nfj_reset_gfuns(nfj_t* p) {
  switch (p->constructor) {
    case PAR:
    case SEQ:
    case PLUS:
      nfj_reset_gfuns(p->content.args._0);
      nfj_reset_gfuns(p->content.args._1);
      break;
    case LOOP:
      nfj_reset_gfuns(p->content.arg);
    case ZERO:
    case ATOM:
      break;
  }
  fmpz_poly_clear(p->gfun);
  fmpz_poly_init(p->gfun);
}

void nfj_reset_prefix_gfuns(nfj_t* p) {
  switch (p->constructor) {
    case PAR:
    case SEQ:
    case PLUS:
      nfj_reset_prefix_gfuns(p->content.args._0);
      nfj_reset_prefix_gfuns(p->content.args._1);
      break;
    case LOOP:
      nfj_reset_prefix_gfuns(p->content.arg);
    case ZERO:
    case ATOM:
      break;
  }
  fmpz_poly_clear(p->prefix_gfun);
  fmpz_poly_init(p->prefix_gfun);
}

void nfj_clear_all(nfj_t* p) {
  switch (p->constructor) {
    case PAR:
    case SEQ:
    case PLUS:
      nfj_clear_all(p->content.args._0);
      nfj_clear_all(p->content.args._1);
      break;
    case LOOP:
      nfj_clear_all(p->content.arg);
    case ZERO:
    case ATOM:
      break;
  }
  fmpz_poly_clear(p->gfun);
  fmpz_poly_clear(p->prefix_gfun);
  free(p);
}

long int nfj_size(const nfj_t* p) {
  switch (p->constructor) {
    case ZERO:
      return 0;
    case ATOM:
      return 1;
    case PAR:
    case SEQ:
    case PLUS:
      return 1 + nfj_size(p->content.args._0) + nfj_size(p->content.args._1);
    default: // LOOP
      return 1 + nfj_size(p->content.arg);
  }
}
