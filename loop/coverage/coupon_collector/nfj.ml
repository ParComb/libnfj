type t =
  | Zero
  | Atom of int
  | Par of t * t
  | Seq of t * t
  | Plus of t * t
  | Loop of t

let rec nullable = function
  | Zero | Loop _ -> true
  | Atom _ -> false
  | Par (p, q) | Seq (p, q) -> nullable p && nullable q
  | Plus (p, q) -> nullable p || nullable q

let rec one_step = function
  | Zero -> []
  | Atom _ -> [Zero]
  | Plus (p, q) -> one_step p @ one_step q
  | Par (p, q) ->
    let ps = List.map (function Zero -> q | p' -> Par (p', q)) (one_step p) in
    let qs = List.map (function Zero -> p | q' -> Par (p, q')) (one_step q) in
    ps @ qs
  | Seq (p, q) ->
    let ps = List.map (function Zero -> q | p' -> Seq (p', q)) (one_step p) in
    if nullable p
    then ps @ one_step q
    else ps
  | Loop p as q ->
    List.map (function Zero -> q | p' -> Seq (p', q)) (one_step p)
