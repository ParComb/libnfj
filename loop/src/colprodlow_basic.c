#include <flint/fmpz_poly.h>

void _fmpz_poly_extra_colprodlow_basic(fmpz* res,
                                       const fmpz* poly1, slong len1,
                                       const fmpz* poly2, slong len2, slong n)
{
  fmpz_t b, t;
  fmpz_init(b);
  fmpz_init(t);

  for (slong i = 0; i < FLINT_MIN(n, len1 + len2 - 1); i++)
    fmpz_zero(res + i);

  for (slong i = 0; i < FLINT_MIN(len1, n); i++) {
    fmpz_one(b);
    for (slong j = 0; j < FLINT_MIN(len2, n - i); j++) {
      fmpz_mul(t, poly1 + i, poly2 + j);
      fmpz_addmul(res + i + j, b, t);
      fmpz_mul_si(b, b, i + j + 1);
      fmpz_divexact_si(b, b, j + 1);
    }
  }

  fmpz_clear(b);
  fmpz_clear(t);
}

void fmpz_poly_extra_colprodlow_basic(fmpz_poly_t res, const fmpz_poly_t poly1,
                                      const fmpz_poly_t poly2, slong n)
{
  if (res == poly1 || res == poly2) {
    fmpz_poly_t t;
    fmpz_poly_init2(t, n);
    _fmpz_poly_extra_colprodlow_basic(t->coeffs,
                                      poly1->coeffs, poly1->length,
                                      poly2->coeffs, poly2->length, n);
    fmpz_poly_swap(res, t);
    fmpz_poly_clear(t);
  } else {
    fmpz_poly_fit_length(res, n);
    _fmpz_poly_extra_colprodlow_basic(res->coeffs,
                                      poly1->coeffs, poly1->length,
                                      poly2->coeffs, poly2->length, n);
  }

  _fmpz_poly_set_length(res, n);
  _fmpz_poly_normalise(res);
}
