type t

val compare : t -> t -> int
val equal : t -> t -> bool
val hash : t -> int
val unsafe_of_string : string -> t

val fresh : string -> t
val pp : Format.formatter -> t -> unit

module Set : Set.S with type elt = t
module Map : Map.S with type key = t
