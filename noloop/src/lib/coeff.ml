type side = L | R

type t = {value: Z.t; desc: desc}
and desc =
  | Const
  | Y of int * side
  | Sum of t * t
  | Prod of t * t

let rec next_binom b i j0 j =
  if j0 = j then b
  else
    let b = Z.div (Z.mul (Z.of_int (i - j0)) b) (Z.of_int (j0 + 1)) in
    next_binom b i (j0 + 1) j


let const n = {value = n; desc = Const}
let zero = const Z.zero
let one = const Z.one
let y uid side = {value = Z.one; desc = Y (uid, side)}

let pp_side fmt = function
  | L -> Format.pp_print_string fmt "L"
  | R -> Format.pp_print_string fmt "R"

let rec pp fmt c = match c.desc with
  | Const -> Z.pp_print fmt c.value
  | Y (uid, side) -> Format.fprintf fmt "y_{%d,%a}" uid pp_side side
  | Sum (x, y) -> Format.fprintf fmt "(%a + %a)" pp x pp y
  | Prod (x, y) -> Format.fprintf fmt "%a %a" pp x pp y

let is_null c = Z.(equal zero) c.value

let add x y =
  if is_null x then y
  else if is_null y then x
  else
    let value = Z.add x.value y.value in
    match x.desc, y.desc with
    | Const, Const -> const value
    | _ -> {value; desc = Sum (x, y)}

let mul x y =
  if is_null x || is_null y then zero
  else if x = one then y
  else if y = one then x
  else
    let value = Z.mul x.value y.value in
    match x.desc, y.desc with
    | Const, Const -> const value
    | _ -> {value; desc = Prod (x, y)}
