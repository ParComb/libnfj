type t = {
  input: string option;
  sample_run: int;
  show_gfun: bool;
  quiet: bool;
}

let make ?input ~sample_run ~show_gfun ~quiet () =
  if sample_run < 0 then invalid_arg "sample-run must be nonnegative";
  { input; sample_run; show_gfun; quiet }
