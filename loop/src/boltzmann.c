#include <stdint.h>
#include "../nfj.h"
#include "xoshiro256p.h"

// The grammar used for the Boltzmann random generation of NFJ programs:
// F ::= a
//     | (F ∥ F)
//     | (F; F)
//     | (F + F)
//     | (0 + F)
//     | (0 + F)
//     | F*

// Multivariate generating function with weights for loops and 0+_ nodes:
// F(z, u, v) = z + 3zF² + 2uzF + vzF

// Tuning of the parameters to get about 10% of choice nodes with a zero below
// and 10% of loops.
static const double z = 0.267024499500203;
static const double f = 0.577350269189613;
static const double u = 0.0468121839883482;
static const double v = 0.187248735953391;

void nfj_seed_rng(uint64_t seed) {xoshiro256p_seed(seed);}

static long int nfj_free_sim(long int size_max) {
  long int todo = 1;
  long int size = 0;

  while (todo && (size <= size_max)) {
    double x = xoshiro256p_double() * f;
    size++;
    if (x < z) todo--;
    else if (x < z + 3 * z * f * f) todo++;
  }

  return size;
}

static void nfj_free_gen(nfj_t* p) {
  double x = xoshiro256p_double() * f;

  x -= z;
  if (x < 0) {
    nfj_init_atom(p);
    return;
  }

  if (x < 3 * z * f * f) {
    nfj_t* q = malloc(sizeof(nfj_t));
    nfj_t* r = malloc(sizeof(nfj_t));
    nfj_free_gen(q);
    nfj_free_gen(r);
    if (x < z * f * f) nfj_init_par(p, q, r);
    else if (x < 2 * z * f * f) nfj_init_seq(p, q, r);
    else nfj_init_plus(p, q, r);
    return;
  }

  nfj_t* q = malloc(sizeof(nfj_t));
  nfj_free_gen(q);

  x -= 3 * z * f * f + v * z * f;
  if (x < 0) {
    nfj_init_loop(p, q);
    return;
  }

  nfj_t* zero = malloc(sizeof(nfj_t));
  nfj_init_zero(zero);
  if (x < u * z * f) nfj_init_plus(p, zero, q);
  else nfj_init_plus(p, q, zero);
}

long int nfj_random(nfj_t* p, long int size_min, long int size_max) {
  long int size = -1;
  uint64_t state[4] = {0,0,0,0};

  while ((size < size_min) | (size > size_max)) {
    xoshiro256p_getstate(state);
    size = nfj_free_sim(size_max);
  }

  xoshiro256p_setstate(state);
  nfj_free_gen(p);
  return size;
}
