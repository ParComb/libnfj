// Random generation toolbox for the recursive method.

#include <flint/fmpz.h>
#include <flint/fmpz_poly.h>

#include <flint/flint.h> // flint_rand_t, FLINT_MAX
// XXX. Flint seems to use GMP's Mersenne Twister internaly.
//      This is not great: https://arxiv.org/abs/1910.06437
//      It would be nice to replace it with more modern and robust RNG.
//      xoshiro256++ would be nice.

#include "flint/ulong_extras.h" // n_randint
// XXX. After reading the source, n_randint seems to rely on two congruential
//      generators + some shuffling. Congruential generators are *bad*.

#include "recmeth.h"

// Uniform interleaving of the [k] first elements of a pool of length [len]
// with the [n - k] remaining elements.
void pool_shuffle(flint_rand_t state, int len, int k, pool* labels)
{
  unsigned int i = k;
  unsigned int j = len - k;

  for (int i = 0; i < len; i++)
    labels->backup[i] = labels->buf[i];

  while (i > 0 && j > 0) {
    if (n_randint(state, i + j) < i) {
      labels->buf[k - i] = labels->backup[len - i - j];
      i--;
    } else {
      labels->buf[len - j] = labels->backup[len - i - j];
      j--;
    }
  }
  while (j > 0) {
    labels->buf[len - j] = labels->backup[len - i - j];
    j--;
  }
  while (i > 0) {
    labels->buf[k - i] = labels->backup[len - i - j];
    i--;
  }
}

// Bernoulli variable of parameter [a/b].
// [a] may be NULL in which case it is interpreted as 0.
int bernoulli(flint_rand_t state, const fmpz* a, const fmpz* b) {
  if (a == NULL) return 0;
  if (fmpz_equal(a, b)) return 1;

  fmpz_t r;
  fmpz_init(r);

  fmpz_randm(r, state, b);
  int res = fmpz_cmp(r, a) < 0;

  fmpz_clear(r);
  return res;
}

// ---[ Recursive method toolbox ]---------------

// Return k with probability p_k q_{n-k} binom(n, k) / tot
slong recmeth_prod(flint_rand_t state, const fmpz_t tot, const fmpz_poly_t p,
                   const fmpz_poly_t q, slong n) {
  fmpz_t r;
  fmpz_init(r);
  fmpz_randm(r, state, tot);

  slong dp = fmpz_poly_degree(p);
  slong dq = fmpz_poly_degree(q);

  slong j = FLINT_MAX(0, FLINT_MIN(n - dq, n - dp));
  while (1) {
    if (j <= dp && n - j <= dq) {
      fmpz_submul(r,
                  fmpz_poly_get_coeff_ptr(p, j),
                  fmpz_poly_get_coeff_ptr(q, n - j));
      if (fmpz_cmp_si(r, 0) < 0) {
        break;
      }
    }
    if (j <= dq && n - j <= dp) {
      fmpz_submul(r,
                  fmpz_poly_get_coeff_ptr(q, j),
                  fmpz_poly_get_coeff_ptr(p, n - j));
      if (fmpz_cmp_si(r, 0) < 0) {
        j = n - j;
        break;
      }
    }
    j++;
  }

  fmpz_clear(r);
  return j;
}

// Same as _recmeth_prod but cannot p_0 is treated as zero.
slong recmeth_prod_nonzero(flint_rand_t state, const fmpz_t tot,
                           const fmpz_poly_t p, const fmpz_poly_t q, slong n) {
  fmpz_t r;
  fmpz_init(r);
  fmpz_randm(r, state, tot);

  slong dp = fmpz_poly_degree(p);
  slong dq = fmpz_poly_degree(q);
  slong j = FLINT_MAX(0, FLINT_MIN(n - dq, n - dp));

  if (j == 0) {
    if (n - j <= dp) {
      fmpz_submul(r,
                  fmpz_poly_get_coeff_ptr(q, j),
                  fmpz_poly_get_coeff_ptr(p, n - j));
      if (fmpz_cmp_si(r, 0) < 0) {
        fmpz_clear(r);
        return (n - j);
      }
    }
    j++;
  }

  while (1) {
    if (j <= dp && n - j <= dq) {
      fmpz_submul(r,
                  fmpz_poly_get_coeff_ptr(p, j),
                  fmpz_poly_get_coeff_ptr(q, n - j));
      if (fmpz_cmp_si(r, 0) < 0) {
        break;
      }
    }
    if (j <= dq && n - j <= dp) {
      fmpz_submul(r,
                  fmpz_poly_get_coeff_ptr(q, j),
                  fmpz_poly_get_coeff_ptr(p, n - j));
      if (fmpz_cmp_si(r, 0) < 0) {
        j = n - j;
        break;
      }
    }
    j++;
  }

  fmpz_clear(r);
  return j;
}

// Return k with probability p_k q_{n-k} binom(n, k) / tot
slong recmeth_colprod(flint_rand_t state, const fmpz_t tot,
                      const fmpz_poly_t p, const fmpz_poly_t q, slong n) {
  slong dp = fmpz_poly_degree(p);
  slong dq = fmpz_poly_degree(q);
  slong j = FLINT_MAX(0, FLINT_MIN(n - dq, n - dp));

  fmpz_t tmp;
  fmpz_init(tmp);

  fmpz_t r;
  fmpz_init(r);
  fmpz_randm(r, state, tot);

  fmpz_t b;
  fmpz_init(b);
  fmpz_bin_uiui(b, n, j);

  while (1) {
    if (j <= dp && n - j <= dq) {
      fmpz_mul(tmp,
               fmpz_poly_get_coeff_ptr(p, j),
               fmpz_poly_get_coeff_ptr(q, n - j));
      fmpz_submul(r, tmp, b);
      if (fmpz_cmp_si(r, 0) < 0) {
        break;
      }
    }
    if (j <= dq && n - j <= dp) {
      fmpz_mul(tmp,
               fmpz_poly_get_coeff_ptr(q, j),
               fmpz_poly_get_coeff_ptr(p, n - j));
      fmpz_submul(r, tmp, b);
      if (fmpz_cmp_si(r, 0) < 0) {
        j = n - j;
        break;
      }
    }

    // b <- (n - j) * binom(n, j) / (j + 1) = binom(n, j + 1)
    fmpz_mul_si(b, b, n - j);
    fmpz_divexact_si(b, b, j + 1);
    j++;
  }

  fmpz_clear(tmp);
  fmpz_clear(b);
  fmpz_clear(r);
  return j;
}

