#ifndef XOSHIRO256PLUS_H
#define XOSHIRO256PLUS_H

#include <stdint.h>

void xoshiro256p_seed(uint64_t);

void xoshiro256p_getstate(uint64_t[4]);
void xoshiro256p_setstate(const uint64_t[4]);

uint64_t xoshiro256p_next();
double xoshiro256p_double();

#endif
