#ifndef FMPZ_POLY_EXTRA_H
#define FMPZ_POLY_EXTRA_H 1

#include <flint/fmpz_poly.h>

inline slong _slong_max(slong i, slong j) {return (i > j) ? i : j;}
inline slong _slong_min(slong i, slong j) {return (i < j) ? i : j;}

// seq(p) = 1/(1 - (p - p(0)))

void _fmpz_poly_extra_seq_basic(fmpz* res, const fmpz* poly, slong len, slong n);
void fmpz_poly_extra_seq_basic(fmpz_poly_t res, const fmpz_poly_t poly, slong n);

void _fmpz_poly_extra_seq_newton(fmpz* res, const fmpz* poly, slong len, slong n);
void fmpz_poly_extra_seq_newton(fmpz_poly_t res, const fmpz_poly_t poly, slong n);

void fmpz_poly_extra_seq(fmpz_poly_t, const fmpz_poly_t, slong);

// Coloured product

void _fmpz_poly_extra_colprodlow_basic(fmpz* res, const fmpz* poly1, slong len1, const fmpz* poly2, slong len2, slong n);
void fmpz_poly_extra_colprodlow_basic(fmpz_poly_t, const fmpz_poly_t, const fmpz_poly_t, slong);

void fmpz_poly_extra_colprodlow_egf(fmpz_poly_t, const fmpz_poly_t, const fmpz_poly_t, slong);

void fmpz_poly_extra_colprodlow(fmpz_poly_t, const fmpz_poly_t, const fmpz_poly_t, slong);

#endif
