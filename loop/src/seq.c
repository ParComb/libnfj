#include <flint/fmpz_poly.h>
#include "fmpz_poly_extra.h"

void fmpz_poly_extra_seq(fmpz_poly_t p, const fmpz_poly_t q, slong n) {
  // Seems fine like this but better performance can probably be achieved via
  // finer tuning based proper benchmarking.
  if (n < 15 || fmpz_poly_length(q) < 5)
    fmpz_poly_extra_seq_basic(p, q, n);
  else
    fmpz_poly_extra_seq_newton(p, q, n);
}
