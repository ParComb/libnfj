type t = string

let compare = String.compare
let equal a b = compare a b = 0
let hash = Hashtbl.hash
let unsafe_of_string s = s


let names = Hashtbl.create 17

let allocate name =
  assert (not (Hashtbl.mem names name)) ;
  Hashtbl.add names name ()

let allocated name =
  Hashtbl.mem names name


let is_digit c =
  Char.code '0' <= Char.code c && Char.code c <= Char.code '9'

let remove_trailing_digits (s : string) : string =
  let n = ref (String.length s) in
  while !n > 0 && is_digit s.[!n-1] do n := !n-1 done;
  (* We assume that there is at least one non-digit character in the string. *)
  assert (!n > 0);
  String.sub s 0 !n

let fresh hint =
  if not (allocated hint) then begin
    allocate hint ;
    hint
  end else
    let prefix = remove_trailing_digits hint in
    if not (allocated prefix) then begin
      allocate prefix ;
      prefix
    end else begin
      let i = ref 0 in
      while allocated (prefix ^ (string_of_int !i)) do
        incr i
      done ;
      let name = prefix ^ (string_of_int !i) in
      allocate name ;
      name
    end


(** Sets of atoms *)
module Set = Set.Make(struct
  type t = string
  let compare = String.compare
end)

(** Maps using atoms as keys *)
module Map = Map.Make(struct
  type t = string
  let compare = String.compare
end)


(** Pretty printing *)
let pp fmt atom =
  let len = String.length atom in
  if atom.[len - 1] = '0' && not (is_digit atom.[len -2]) then
    Format.pp_print_string fmt (remove_trailing_digits atom)
  else
    Format.pp_print_string fmt atom
