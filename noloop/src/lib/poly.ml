type t = Coeff.t array

let pp fmt poly =
  for i = 0 to Array.length poly - 2 do
    if not (Coeff.is_null poly.(i)) then
      Format.fprintf fmt "%a z^%d + " Coeff.pp poly.(i) i
  done;
  let n = Array.length poly - 1 in
  Format.fprintf fmt "%a z^%d" Coeff.pp poly.(n) n

let z () = [|Coeff.zero; Coeff.one|]

let mul p q =
  let dp = Array.length p - 1 in
  let dq = Array.length q - 1 in
  let degree = dp + dq in
  Array.init
    (degree + 1)
    (fun i ->
      let j0 = max 0 (i - dq) in
      let jmax = min i dp in
      let c = ref Coeff.zero in
      for j = j0 to jmax do
        c := Coeff.add !c (Coeff.mul p.(j) q.(i - j))
      done;
      !c)

let binom =
  let rec aux n k =
    if k = 0 then Z.one
    else
      Z.divexact
        (Z.mul (Z.of_int n) (aux (n - 1) (k - 1)))
        (Z.of_int k)
  in
  fun n k ->
    if k < 0 || k > n then Z.zero
    else aux n (min k (n - k))

let colored_product p q =
  let dp = Array.length p - 1 in
  let dq = Array.length q - 1 in
  let degree = dp + dq in
  Array.init
    (degree + 1)
    (fun i ->
      let j0 = max 0 (i - dq) in
      let jmax = min i dp in
      let c = ref Coeff.zero in
      let b = ref (binom i j0) in
      for j = j0 to jmax do
        if not (Coeff.is_null p.(j)) && not (Coeff.is_null q.(i - j)) then
          c := Coeff.(add !c (mul (const !b) (mul p.(j) q.(i - j))));
          b := Z.divexact (Z.mul (Z.of_int (i - j)) !b) (Z.of_int (j + 1))
      done;
      !c)

let add uid p q =
  let dp = Array.length p - 1 in
  let dq = Array.length q - 1 in
  let degree = max dp dq in
  Array.init
    (degree + 1)
    (fun i ->
      let lhs = if i <= dp then Coeff.(mul (y uid L) p.(i)) else Coeff.zero in
      let rhs = if i <= dq then Coeff.(mul (y uid R) q.(i)) else Coeff.zero in
      Coeff.add lhs rhs)
