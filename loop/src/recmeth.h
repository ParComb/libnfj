#ifndef NFJ_RECMETH_H
#define NFJ_RECMETH_H 1

#include <flint/fmpz.h>
#include <flint/fmpz_poly.h>

#include <flint/flint.h> // flint_rand_t

typedef struct {
  int* buf;
  int* backup;
} pool;

void pool_shuffle(flint_rand_t, int, int, pool*);
int bernoulli(flint_rand_t, const fmpz*, const fmpz*);
slong recmeth_prod(flint_rand_t, const fmpz_t, const fmpz_poly_t, const fmpz_poly_t, slong);
slong recmeth_prod_nonzero(flint_rand_t, const fmpz_t, const fmpz_poly_t, const fmpz_poly_t, slong);
slong recmeth_colprod(flint_rand_t, const fmpz_t, const fmpz_poly_t, const fmpz_poly_t, slong);

#endif
