let enabled_tests = [
  TestParsing.parse_no_error
]

let () =
  List.iter (fun f -> f ()) enabled_tests
