#include <flint/fmpz_poly.h>

static void borel(fmpz_poly_t res, fmpz_t f, const fmpz_poly_t poly) {
  fmpz_one(f);

  for (slong i = fmpz_poly_length(poly) - 1; i > 0; i--) {
    fmpz_mul(res->coeffs + i, poly->coeffs + i, f);
    fmpz_mul_si(f, f, i);
  }
  fmpz_mul(res->coeffs, poly->coeffs, f);
}

static void laplace_divexact(fmpz_poly_t p, fmpz_t f, const fmpz_t d) {
  fmpz_one(f);

  fmpz_divexact(p->coeffs, p->coeffs, d);
  for (slong i = 1; i < p->length; i++) {
    fmpz_mul_si(f, f, i);
    fmpz_mul(p->coeffs + i, p->coeffs + i, f);
    fmpz_divexact(p->coeffs + i, p->coeffs + i, d);
  }
}

void fmpz_poly_extra_colprodlow_egf(fmpz_poly_t res, const fmpz_poly_t poly1,
                                    const fmpz_poly_t poly2, slong n)
{
  fmpz_t f1;
  fmpz_init(f1);
  fmpz_poly_t t1;
  fmpz_poly_init2(t1, fmpz_poly_length(poly1));
  borel(t1, f1, poly1);
  t1->length = poly1->length;

  fmpz_t f2;
  fmpz_init(f2);
  fmpz_poly_t t2;
  fmpz_poly_init2(t2, fmpz_poly_length(poly2));
  borel(t2, f2, poly2);
  t2->length = poly2->length;

  fmpz_poly_mullow(res, t1, t2, n);
  fmpz_mul(f1, f1, f2);
  laplace_divexact(res, f2, f1);

  fmpz_clear(f1);
  fmpz_clear(f2);
  fmpz_poly_clear(t1);
  fmpz_poly_clear(t2);
}
