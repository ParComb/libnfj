(** Boltzmann sampler of NFJ programs *)

open Libnfj

(** Grammar
  F ::= a
      | (F || F)
      | (F; F)
      | (F + F)

  Combinatorial spec: F = z + 3 * F^2

  Tuning of the parameter z for singular sampling:
*)

let z = 1. /. 12.
let f = 1. /. 6. (* = 2z *)

let free_size ~size_max =
  let rec aux size todo =
    if size > size_max || todo = 0 then size
    else
      if Random.float f < z then aux (size + 1) (todo - 1)
      else aux size (todo + 1)
  in
  aux 0 1

let fresh_name =
  let c = ref 'a' in
  fun () ->
    let x = !c in
    if x = 'z' then c := 'a'
    else c := Char.chr (Char.code x + 1);
    String.make 1 x

type task = Gen | Par | Seq | Plus

let unwrap = function
  | [x] -> x
  | _ -> invalid_arg "unwrap"

let pop2 = function
  | x :: y :: xs -> x, y, xs
  | _ -> invalid_arg "pop2"

let fresh_uuid =
  let c = ref (-1) in
  fun () ->
    incr c;
    !c

let free_gen =
  let rec aux generated todo = match todo with
    | [] -> unwrap generated
    | Gen :: todo ->
      let r = Random.float f in
      if r < z then
        let a = Nfj.atom (Atom.unsafe_of_string (fresh_name ())) in
        aux (a :: generated) todo
      else if r < z +. 1. *. f *. f then
        aux generated (Gen :: Gen :: Par :: todo)
      else if r < z +. 2. *. f *. f then
        aux generated (Gen :: Gen :: Seq :: todo)
      else
        aux generated (Gen :: Gen :: Plus :: todo)
    | Par :: todo ->
      let p, q, generated = pop2 generated in
      aux (Nfj.par p q :: generated) todo
    | Seq :: todo ->
      let p, q, generated = pop2 generated in
      aux (Nfj.seq p q :: generated) todo
    | Plus :: todo ->
      let p, q, generated = pop2 generated in
      aux (Nfj.plus (fresh_uuid ()) p q :: generated) todo
  in
  fun () ->
    aux [] [Gen]

(** {2 High-level interface to the generator} *)

(** Generate one NFJ term in a size window *)
let rec gen ~size_min ~size_max =
  let state = Random.get_state () in
  let s = free_size ~size_max in
  if s < size_min || s > size_max then
    gen ~size_min ~size_max
  else begin
    Random.set_state state;
    s, free_gen ()
  end
