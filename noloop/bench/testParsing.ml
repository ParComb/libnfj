open Libcbl


let rec iterdirs start f =
  if Sys.is_directory start then
    Sys.readdir start
    |> Array.iter (fun filename -> iterdirs (Filename.concat start filename) f)
  else if Filename.extension start = ".cbl" then
    f start


let parse_file filename =
  let ic = open_in filename in
  try
    Lexing.from_channel ic |> Parser.file Lexer.token
  with err ->
    (close_in_noerr ic ; raise err)


let parse_no_error () =
  (* XXX. hackish *)
  iterdirs "../../../examples" (fun f -> ignore (parse_file f))
