#include <flint/fmpz_poly.h>
#include "fmpz_poly_extra.h"

void fmpz_poly_extra_colprodlow(fmpz_poly_t p, const fmpz_poly_t q,
                                const fmpz_poly_t r, slong n) {
  // Seems fine like this but better performance can probably be achieved via
  // finer tuning based proper benchmarking.
  if (fmpz_poly_degree(q) < 10 || fmpz_poly_degree(r) < 10)
    fmpz_poly_extra_colprodlow_basic(p, q, r, n);
  else
    fmpz_poly_extra_colprodlow_egf(p, q, r, n);
}
