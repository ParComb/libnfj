#include "flint/flint.h" // FLINT_MIN
#include "flint/fmpz.h"
#include "flint/fmpz_poly.h"
#include "fmpz_poly_extra.h"

void _fmpz_poly_extra_seq_basic(fmpz* res, const fmpz* poly, slong len, slong n)
{
  fmpz_one(res);
  for (int i = 1; i < n; i++) {
    fmpz_zero(res + i);
    for (int j = 1; j < FLINT_MIN(len, i + 1); j++) {
      fmpz_addmul(res + i, poly + j, res + i - j);
    }
  }
}

void fmpz_poly_extra_seq_basic(fmpz_poly_t res, const fmpz_poly_t poly, slong n)
{
  if (res == poly) {
    fmpz_poly_t t;
    fmpz_poly_init2(t, n);
    _fmpz_poly_extra_seq_basic(t->coeffs, poly->coeffs, poly->length, n);
    fmpz_poly_swap(res, t);
    fmpz_poly_clear(t);
  } else {
    fmpz_poly_fit_length(res, n);
    _fmpz_poly_extra_seq_basic(res->coeffs, poly->coeffs, poly->length, n);
  }

  _fmpz_poly_set_length(res, n);
  _fmpz_poly_normalise(res);
}
