#ifndef NFJ_INTERNALS_H
#define NFJ_INTERNALS_H 1

#include "recmeth.h"
#include "../nfj.h"

void _nfj_rand_exec(sp_t*, flint_rand_t, const nfj_t*, int, pool*);

#endif
