(** Non-deterministic Fork-Join programs with loops. *)

type uid = int

type t = {size: int; desc: desc}
and desc =
  | Atom of Atom.t
  | Par of t * t
  | Seq of t * t
  | Plus of uid * t * t

let atom a = {size = 1; desc = Atom a}
let par p q = {size = p.size + q.size; desc = Par (p, q)}
let seq p q = {size = p.size + q.size; desc = Seq (p, q)}
let plus uid p q = {size = p.size + q.size; desc = Plus (uid, p, q)}

(** {2 Pretty printing} *)

let rec pp fmt t = match t.desc with
  | Atom a -> Atom.pp fmt a
  | Par (p, q) -> Format.fprintf fmt "(%a || %a)" pp p pp q
  | Seq (p, q) -> Format.fprintf fmt "(%a; %a)" pp p pp q
  | Plus (_, p, q) -> Format.fprintf fmt "(%a + %a)" pp p pp q

(** {2 Generating function} *)

let rec gfun t = match t.desc with
  | Atom _ -> Poly.z ()
  | Par (p, q) -> Poly.colored_product (gfun p) (gfun q)
  | Seq (p, q) -> Poly.mul (gfun p) (gfun q)
  | Plus (uid, p, q) -> Poly.add uid (gfun p) (gfun q)
