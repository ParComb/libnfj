#include <stdio.h>
#include <flint/fmpz_poly.h>
#include <flint/fmpz.h>

#include "fmpz_poly_extra.h"
#include "../nfj.h"

void nfj_compute_gfuns(nfj_t* p, long int n) {
  if (fmpz_poly_length(p->gfun) > 0) return;

  switch (p->constructor) {
    case ZERO:
      fmpz_poly_one(p->gfun);
      break;
    case ATOM:
      fmpz_poly_set_coeff_ui(p->gfun, 1, 1);
      break;
    case PAR:
      nfj_compute_gfuns(p->content.args._0, n);
      nfj_compute_gfuns(p->content.args._1, n);
      fmpz_poly_extra_colprodlow(p->gfun, p->content.args._0->gfun,
                                 p->content.args._1->gfun, n + 1);
      break;
    case SEQ:
      nfj_compute_gfuns(p->content.args._0, n);
      nfj_compute_gfuns(p->content.args._1, n);
      fmpz_poly_mullow(p->gfun, p->content.args._0->gfun, p->content.args._1->gfun, n + 1);
      break;
    case PLUS:
      nfj_compute_gfuns(p->content.args._0, n);
      nfj_compute_gfuns(p->content.args._1, n);
      fmpz_poly_add(p->gfun, p->content.args._0->gfun, p->content.args._1->gfun);
      if (fmpz_poly_get_coeff_si(p->content.args._0->gfun, 0) == 1
          && fmpz_poly_get_coeff_si(p->content.args._1->gfun, 0) == 1)
        fmpz_poly_set_coeff_si(p->gfun, 0, 1);
      break;
    case LOOP:
      nfj_compute_gfuns(p->content.arg, n);
      fmpz_poly_extra_seq(p->gfun, p->content.arg->gfun, n + 1);
      break;
  }
}
