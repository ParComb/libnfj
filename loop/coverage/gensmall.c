#include "../nfj.h"
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char* argv[]) {
  int size, n;
  if (argc == 3) {
    size = atoi(argv[1]);
    n = atoi(argv[2]);
  } else {
    fprintf(stderr, "usage: %s SIZE N\n", argv[0]);
    return 1;
  }

  nfj_seed_rng(0x252c5881b39b);
  nfj_t* p = malloc(sizeof(nfj_t));
  nfj_random(p, size, size);
  nfj_compute_gfuns(p, n);
  nfj_compute_prefix_gfuns(p, n);
  for (int i = 0; i <= n; i++) {
    fmpz* ptr = fmpz_poly_get_coeff_ptr(p->prefix_gfun, i);
    if (ptr == NULL)
      printf("0");
    else
      fmpz_print(ptr);
    printf(" ");
  }
  printf("\n");
  nfj_pretty_print(stdout, p);
  printf("\n");
  nfj_clear_all(p);

  return 0;
}

