#include <malloc.h>
#include "../nfj.h"

long int sp_size(const sp_t* p) {
  if (p->constructor == SP_ATOM)
    return 1;
  else
    return sp_size(p->content.args._0) + sp_size(p->content.args._1);
}


void sp_clear_all(sp_t* p) {
  if (p->constructor != SP_ATOM) {
    sp_clear_all(p->content.args._0);
    sp_clear_all(p->content.args._1);
  }
  free(p);
}

