#include <malloc.h>
#include <stdio.h>

#include "recmeth.h"
#include "../nfj.h"
#include "internals.h"


// ---[ Random generation of executions ]--------

void _nfj_rand_exec(sp_t* q, flint_rand_t state, const nfj_t* p, int len,
                    pool* labels) {
  if (!len) return;
  int k;
  switch (p->constructor) {
    case ATOM:
      q->constructor = SP_ATOM;
      q->content.atom.id = p->content.id;
      q->content.atom.label = labels->buf[0];
      labels->buf++;
      break;
    case PLUS:
      if (bernoulli(state,
                    fmpz_poly_get_coeff_ptr(p->content.args._0->gfun, len),
                    fmpz_poly_get_coeff_ptr(p->gfun, len)))
        _nfj_rand_exec(q, state, p->content.args._0, len, labels);
      else
        _nfj_rand_exec(q, state, p->content.args._1, len, labels);
      break;
    case PAR:
      k = recmeth_colprod(state,
                          fmpz_poly_get_coeff_ptr(p->gfun, len),
                          p->content.args._0->gfun,
                          p->content.args._1->gfun,
                          len);
      if (k == 0)
        _nfj_rand_exec(q, state, p->content.args._1, len, labels);
      else if (k == len)
        _nfj_rand_exec(q, state, p->content.args._0, len, labels);
      else {
        pool_shuffle(state, len, k, labels);
        q->constructor = SP_PAR;
        q->content.args._0 = malloc(sizeof(sp_t));
        q->content.args._1 = malloc(sizeof(sp_t));
        _nfj_rand_exec(q->content.args._0, state, p->content.args._0, k, labels);
        _nfj_rand_exec(q->content.args._1, state, p->content.args._1, len - k, labels);
      }
      break;
    case SEQ:
      k = recmeth_prod(state,
                       fmpz_poly_get_coeff_ptr(p->gfun, len),
                       p->content.args._0->gfun,
                       p->content.args._1->gfun,
                       len);
      if (k == 0)
        _nfj_rand_exec(q, state, p->content.args._1, len, labels);
      else if (k == len)
        _nfj_rand_exec(q, state, p->content.args._0, len, labels);
      else {
        q->constructor = SP_SEQ;
        q->content.args._0 = malloc(sizeof(sp_t));
        q->content.args._1 = malloc(sizeof(sp_t));
        _nfj_rand_exec(q->content.args._0, state, p->content.args._0, k, labels);
        _nfj_rand_exec(q->content.args._1, state, p->content.args._1, len - k, labels);
      }
      break;
    case LOOP:
      k = recmeth_prod_nonzero(state,
                               fmpz_poly_get_coeff_ptr(p->gfun, len),
                               p->content.arg->gfun,
                               p->gfun,
                               len);
      if (k == len)
        _nfj_rand_exec(q, state, p->content.arg, k, labels);
      else {
        q->constructor = SP_SEQ;
        q->content.args._0 = malloc(sizeof(sp_t));
        q->content.args._1 = malloc(sizeof(sp_t));
        _nfj_rand_exec(q->content.args._0, state, p->content.arg, k, labels);
        _nfj_rand_exec(q->content.args._1, state, p, len - k, labels);
      }
      break;
    case ZERO: // should not happen
      break;
  }
}

void nfj_rand_exec(sp_t* q, flint_rand_t state, const nfj_t* p, int len) {
  int* buf = malloc(sizeof(int) * len);
  int* backup = malloc(sizeof(int) * len);
  pool labels = {.buf = buf, .backup = backup};

  for (int i = 0; i < len; i++) labels.buf[i] = i + 1;
  _nfj_rand_exec(q, state, p, len, &labels);

  free(buf);
  free(backup);
}

// ---[ Pretty printing of executions ]----------

void sp_pretty_print(FILE* f, const sp_t* q) {
  switch (q->constructor) {
    case SP_ATOM:
      fprintf(f, "a%d", q->content.atom.id);
      if (q->content.atom.label > 0)
        fprintf(f, "^%d", q->content.atom.label);
      break;
    case SP_PAR:
      fprintf(f, "(");
      sp_pretty_print(f, q->content.args._0);
      fprintf(f, " ∥ ");
      sp_pretty_print(f, q->content.args._1);
      fprintf(f, ")");
      break;
    case SP_SEQ:
      fprintf(f, "(");
      sp_pretty_print(f, q->content.args._0);
      fprintf(f, "; ");
      sp_pretty_print(f, q->content.args._1);
      fprintf(f, ")");
      break;
  }
}
