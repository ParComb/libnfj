#ifndef NFJ_H
#define NFJ_H 1

#include <stdio.h>
#include <stdint.h>
#include <flint/flint.h> // flint_rand_t
#include <flint/fmpz_poly.h>

// The AST for NFJ programs

enum nfj_constructor {
  ZERO,
  ATOM,
  PAR,
  SEQ,
  PLUS,
  LOOP,
};

typedef struct nfj_t {
  enum nfj_constructor constructor;
  fmpz_poly_t gfun;
  fmpz_poly_t prefix_gfun;
  union nfj_content {
    int id;
    struct nfj_t* arg;
    struct nfj_args {
      struct nfj_t* _0;
      struct nfj_t* _1;
    } args;
  } content;
} nfj_t;

// Operations on NFJ programs

void nfj_init_zero(nfj_t*);
void nfj_init_atom(nfj_t*);
void nfj_init_par(nfj_t*, nfj_t*, nfj_t*);
void nfj_init_seq(nfj_t*, nfj_t*, nfj_t*);
void nfj_init_plus(nfj_t*, nfj_t*, nfj_t*);
void nfj_init_loop(nfj_t*, nfj_t*);

long int nfj_size(const nfj_t*);

void nfj_pretty_print(FILE*, const nfj_t*);

void nfj_compute_gfuns(nfj_t*, slong n);
void nfj_compute_prefix_gfuns(nfj_t*, slong n);

void nfj_reset_gfuns(nfj_t*);
void nfj_reset_prefix_gfuns(nfj_t*);
void nfj_clear_all(nfj_t*);

void nfj_seed_rng(uint64_t);
long int nfj_random(nfj_t*, long int, long int);

// Executions, as labelled global choices.
// "sp" stands for "series-parallel"

enum sp_constructor {
  SP_ATOM,
  SP_PAR,
  SP_SEQ,
};

typedef struct sp_t {
  enum sp_constructor constructor;
  union sp_content {
    struct sp_atom {
      int id;
      unsigned int label;
    } atom;
    struct sp_args {
      struct sp_t* _0;
      struct sp_t* _1;
    } args;
  } content;
} sp_t;

void sp_clear_all(sp_t*);

long int sp_size(const sp_t*);

void sp_pretty_print(FILE*, const sp_t*);

void nfj_rand_exec(sp_t*, flint_rand_t, const nfj_t*, int);
void nfj_rand_prefix(sp_t*, flint_rand_t, const nfj_t*, int);

#endif
