#include <stdio.h>
#include <flint/flint.h> // flint_rand_t
#include "../nfj.h"

// Tiny example: (a ∥ b*)*
nfj_t* make_example() {
  nfj_t* a = malloc(sizeof(nfj_t));
  nfj_t* b = malloc(sizeof(nfj_t));
  nfj_t* a_star = malloc(sizeof(nfj_t));
  nfj_t* inner = malloc(sizeof(nfj_t));
  nfj_t* p = malloc(sizeof(nfj_t));

  nfj_init_atom(a);
  nfj_init_atom(b);
  nfj_init_loop(a_star, a);
  nfj_init_par(inner, b, a_star);
  nfj_init_loop(p, inner);

  return p;
}

// Randomly generated example
nfj_t* generate_example(int target) {
  nfj_t* p = malloc(sizeof(nfj_t));
  long int size = nfj_random(p, target, target + target/20);
  long int real_size = nfj_size(p);
  if (size != real_size)
    printf("Woops! %ld != %ld\n", size, real_size);
  return p;
}

void test(flint_rand_t state, nfj_t* p, slong n) {
  nfj_pretty_print(stdout, p);
  printf("\n");

  // Compute the generating function executions.
  nfj_compute_gfuns(p, n);
  fmpz_poly_print(p->gfun);
  printf("\n");

  // Compute the generating function of execution prefixes.
  nfj_compute_prefix_gfuns(p, n);
  fmpz_poly_print(p->prefix_gfun);
  printf("\n");

  // Generate a random execution.
  sp_t* e = malloc(sizeof(sp_t));
  nfj_rand_exec(e, state, p, n);
  sp_pretty_print(stdout, e);
  printf("\n");

  // Generate a random prefix.
  sp_t* pref = malloc(sizeof(sp_t));
  nfj_rand_prefix(pref, state, p, n / 2);
  sp_pretty_print(stdout, pref);
  printf("\n");

  sp_clear_all(e);
  sp_clear_all(pref);
}

void tests(flint_rand_t state) {
  nfj_t* p = make_example();
  test(state, p, 10);

  nfj_t* q = generate_example(50);
  test(state, q, 50);

  nfj_clear_all(p);
  nfj_clear_all(q);
}

int main() {
  flint_rand_t state;
  flint_randinit(state);
  flint_randseed(state, 42242424242442UL, 0xdeadbeef3737737);
  nfj_seed_rng(4242424242424242424UL);

  tests(state);

  return 0;
}

