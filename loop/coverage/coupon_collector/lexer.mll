{
  open Lexing
  open Parser

  let newline lexbuf =
    let pos = lexbuf.lex_curr_p in
    lexbuf.lex_curr_p <-
    { pos with pos_lnum = pos.pos_lnum + 1; pos_bol = pos.pos_cnum }
}

let lowercase = ['a'-'z']
let uppercase = ['A'-'Z']
let alpha = lowercase | uppercase
let digit = ['0'-'9']

let lowident = lowercase (alpha | '_' | digit | '\'')*

rule token = parse
  | '\n'                { newline lexbuf; token lexbuf }
  | [' ' '\t' '\r']+    { token lexbuf }
  | lowident as id      { IDENT id }
  | '0'                 { ZERO }
  | "||"                { PAR }
  | '+'                 { PLUS }
  | ';'                 { SEMICOLON }
  | '*'                 { STAR }
  | '('                 { LPAR }
  | ')'                 { RPAR }
  | eof                 { EOF }
  | _ as c              { failwith (Format.sprintf "Unknown character: '%c' (%d)" c (int_of_char c)) }
