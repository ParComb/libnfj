CC = cc
AR = ar
RANLIB = ranlib

CFLAGS = -std=c11 -flto -O2 -Wall -Wextra -pedantic
LDFLAGS = -s
