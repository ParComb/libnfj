open Libnfj

(** {2 Command line parsing *)

let parse_args () =
  let usage = Format.sprintf "usage: %s [options] file.cbl" Sys.argv.(0) in

  let input: string option ref = ref None in
  let sample_run = ref 1 in
  let show_gfun = ref false in
  let quiet = ref false in

  let speclist = [
    "-sample-run", Arg.Int (fun n -> sample_run := n), " Sample runs";
    "-show-gfun", Arg.Set show_gfun, " Show the generating function of executions";
    "-quiet", Arg.Set quiet, " Set quiet output";
  ] in

  let extra =
    let cpt = ref 0 in
    fun s ->
      incr cpt;
      if !cpt = 1 then input := Some s
      else raise (Arg.Bad ("Too many arguments"))
  in

  Arg.parse speclist extra usage;
  Options.make
    ?input:!input
    ~sample_run:!sample_run
    ~show_gfun:!show_gfun
    ~quiet:!quiet
    ()

(** {2 Entry point} *)

let rec do_n f n =
  if n > 0 then (f (); do_n f (n - 1))

let () =
  let open Options in
  Random.self_init ();
  let options = parse_args () in

  let message msg =
    let print =
      if not options.quiet then Format.printf "=> %s:@." else ignore
    in
    Format.kasprintf print msg
  in

  let pp_exec =
    Format.pp_print_list
      ~pp_sep:(fun fmt () -> Format.pp_print_char fmt ' ')
      Atom.pp
  in

  let term = IO.parse_input options in

  let gfun = ref [||] in

  if options.sample_run > 0 then begin
    gfun := Nfj.gfun term;
    let sample () =
      let exec = Sampling.unif_exec term !gfun in
      Format.printf "%a@." pp_exec exec
    in
    message "A few random executions";
    do_n sample options.sample_run
  end;

  if options.show_gfun then begin
    if !gfun = [||] then gfun := Nfj.gfun term;
    message "Generating function of executions";
    Format.printf "%a@." Poly.pp !gfun
  end
