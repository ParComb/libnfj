%{
open Nfj

let parse_atom a =
  assert (a.[0] = 'a');
  let id = int_of_string (String.sub a 1 (String.length a - 1)) in
  Atom id
%}

%token ZERO
%token <string> IDENT
%token PAR PLUS SEMICOLON STAR
%token LPAR RPAR
%token EOF

%left PLUS
%left PAR
%left SEMICOLON
%nonassoc STAR

%start file

%type <Nfj.t> file

%%

file: p = term EOF  { p }

term:
| LPAR p = term RPAR            { p }
| ZERO                          { Zero }
| a = IDENT                     { parse_atom a }
| p = term PLUS q = term        { Plus (p, q) }
| p = term PAR q = term         { Par (p, q) }
| p = term SEMICOLON q = term   { Seq (p, q) }
| p = term STAR                 { Loop p}
