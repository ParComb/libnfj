%{
  let fresh_uuid =
    let c = ref (-1) in
    fun () ->
      incr c;
      !c
%}
%token <string> IDENT
%token PAR PLUS SEMICOLON
%token LPAR RPAR
%token EOF

%left PLUS
%left PAR
%left SEMICOLON

%start file

%type <Nfj.t> file

%%

file: p = term EOF  { p }

term:
| LPAR p = term RPAR            { p }
| a = IDENT                     { Nfj.atom (Atom.fresh a) }
| p = term PLUS q = term        { Nfj.plus (fresh_uuid ()) p q }
| p = term PAR q = term         { Nfj.par p q }
| p = term SEMICOLON q = term   { Nfj.seq p q }
